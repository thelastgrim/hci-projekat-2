package main;

import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.staticFiles;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import spark.utils.IOUtils;



public class Main {
	private static String currentDirectory = System.getProperty("user.dir");
	private static String graphPath = currentDirectory + File.separator + "static" + File.separator + "graph.json";
	private static PrintWriter out;

	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static JsonParser jsonParser = new JsonParser();

	public static void main(String[] args) throws IOException {
		port(8080);
		
		staticFiles.externalLocation(new File("./static").getCanonicalPath());
		
		
		
		
		
		post("/upload", (req, res)->{
			
			
			req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("static/source/"));
			Part uploadFile = req.raw().getPart("data");
			String fname = req.raw().getParameter("fname");
			
			if (uploadFile.getSubmittedFileName()!=null) {
				
				try (InputStream inputStream = uploadFile.getInputStream()) {
	                OutputStream outputStream = new FileOutputStream("static/source/" + fname);
	                IOUtils.copy(inputStream, outputStream);
	                outputStream.close();
	            } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//logo = "static/source/" + uploadFile.getSubmittedFileName();	
			}
			
			
			
			return "source/"+fname;	
		});

		post("/saveGraph", (req, res)->{
			String payload = req.body();

			JsonElement jsonElement = jsonParser.parse(payload);
			String jsonString = gson.toJson(jsonElement);

            out = new PrintWriter(new FileWriter(graphPath));
			out.println(jsonString);
			out.flush();
			out.close();

			res.status(200);
			return "Celokupan graf je sačuvan";
		});
		
		
		
		
		
		
		
		
	};

	
	
}
